<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Svadobný salón Ália</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/site.css" rel="stylesheet" />
    
    <link href="../css/photoswipe.css" rel="stylesheet" />


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<div class="header">
  		<div>
  			<img src="/img/logo.png" />
  		</div>
  	</div>
  	
  	<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand active" href="index.html"><img width="46px" height="46px" src="/img/A.png" title="Úvodná stránka"/></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li ><a href="/">O nás </a></li>
        <li><a href="/aktuality">Aktuality</a></li>
        <li ><a href="/nase-sluzby">Naše služby</a></li>
        <li class="active"><a href="/galeria">Galéria</a></li>
        <li><a href="/kontakt">Kontakt</a></li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
      	<li><a style="color:Blue" href="http://facebook.com/alia.trencin" target="_blank"><i>Facebook</i></a></li>
        <li><a href="http://namieru.alia.eu.sk">Šaty na mieru</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
  	
    <div class="container main">
    	<div class="row"> 
    			<div class="col-md-3">
    			<p>
					<h2>Fotogaléria </h2>
				</p>
				<nav class="navbar navbar-default">

    				<!-- Brand and toggle get grouped for better mobile display -->
    				<div class="navbar-header">
      				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
      					Galérie
        				<span class="sr-only">Toggle navigation</span>
        				<span class="glyphicon glyphicon-chevron-down"></span>  				
      				</button>    				
    				</div>

    				<!-- Collect the nav links, forms, and other content for toggling -->
    				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      				<ul class="nav nav-stacked nav-pills">
        				<li id="svadobne-saty"><a href="?katalog=svadobne-saty">Svadobné šaty</a></li>
        				<li id="spolocenske-saty"><a href="?katalog=spolocenske-saty">Spoločenské šaty</a></li>
        				<li id="detske-saty"><a href="?katalog=detske-saty">Detské šaty</a></li>
        				<li id="detske-obleky"><a href="?katalog=detske-obleky">Detské obleky</a></li>
        				<li id="panske-obleky"><a href="?katalog=panske-obleky">Pánske obleky</a></li>
        				<li id="svadobne-doplnky"><a href="?katalog=svadobne-doplnky">Svadobné doplnky</a></li>
        				<li id="bizuteria"><a href="?katalog=bizuteria">Bižutéria</a></li>
        				<li id="vyzdoba"><a href="?katalog=vyzdoba">Výzdoba</a></li>
        				<li id="saty-na-predaj"><a href="?katalog=saty-na-predaj">Šaty na predaj</a></li>
        				<!-- <li id="modne-prehliadky"><a href="?katalog=modne-prehliadky">Módne prehliadky</a></li> -->
        				<li><a id="nove-priestory" href="?katalog=nove-priestory">Nové priestory</a></li>       				
      				</ul>
    				</div><!-- /.navbar-collapse -->
				</nav>
    		</div>  		 		
    			<div class="col-md-9">
    				<?php
					function Printfolder($g)
							{
								$dir="fotky/$g";
								$c=1;
								$files = scandir($dir);
								asort($files);
								foreach ($files as $file) {
    								if ($file != '.' && $file != '..') {
        								echo "<div class=\"responsive img-thumbnail\"><a href=\"?katalog=$g&detail=$file\"> <div class=\"thumb_div\"> <img class=\"thumb\" src=\"$dir/$file/t.jpg\" /></div></a></div>";
										
    								}
								}
								
							}
					
					function Printdetail($g,$d)
							{
								echo "<div class=\"row well\">";
								echo "<a class=\"btn btn-default\" href=\"?katalog=$g\"><span class=\"glyphicon glyphicon-chevron-left \" ></span>Späť</a>";
								echo "<span class=\"pull-right\">Detail:<small> $g-$d </small></span>";
								echo "</br> </br>";
								echo "</div>";
								echo "<div id=\"Gallery\" class=\"center\">";								
								{
								$dir="fotky/$g/$d";
								$c=0;
								if ($handle = opendir($dir)) {    										
    								while (false !== ($entry = readdir($handle))) {
    									if($entry!="." && $entry!=".." && $entry!="t.jpg")
											{					
												echo "<div class=\"gallery-item\"> <a href=\"$dir/$entry\"> <img class=\"responsive img-thumbnail\" src=\"$dir/$entry\" /></a></div>";
											}
        								$c++;
										
    								}
									echo "</div>";
									echo "</br></br></br><a class=\"btn btn-default\" href=\"?katalog=$g\"><span class=\"glyphicon glyphicon-chevron-left \" ></span>Späť</a>";						
    								closedir($handle);
								}
							}
							}

							$g=$_GET["katalog"];
							$d=$_GET["detail"];
							if($d==null && $g==null)
							{		
								Printfolder("svadobne-saty");
							}
							elseif($d==null && $g!=null)
							{
								if($g=="bizuteria" || $g=="vyzdoba" || $g=="nove-priestory")
								{
									Printdetail($g,$d);
								}
								else {
									Printfolder($g);
								}
								
							}
							elseif ($d!=null && $g!=null) {
								Printdetail($g,$d);
							}
						
					
							
															
					?>
    			</div>   		  		
    	</div>
		
    </div>
    <div class="footer">
      <div class="container main center">
      	<div class="row">
      		<div class="col-md-4">
      			<p><a href="#"> <span class="glyphicon glyphicon-chevron-up"></span>Hore</a></p>
      		</div>
      		<div class="col-md-4">
      			<p>Ália &copy; 2015</p>
      		</div>
      		<div class="col-md-4">
      			<p>Web design <a href="mailto:matoboor@gmail.com">Martin Boor</a> </p>
      		</div>
      	</div>

      </div>

    </div>
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery-1.11.2.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="/js/simple-inheritance.min.js"></script>
		<script type="text/javascript" src="/js/code-photoswipe-1.0.11.min.js"></script>
    
    <!-- Responsive slides -->
    <script>
  			$(document).ready(function(){
  				jQuery('#<?php
  							if($g==null){
  								echo "svadobne-saty";
							}
							else {
								echo $g;
							} 
  								
  							?>').addClass('active');
  							
  				
			});
	</script>
	    <script>
  			document.addEventListener('DOMContentLoaded', function(){
			Code.photoSwipe('a', '#Gallery');
			}, true);
	</script>
  </body>
</html>